# MedDelivery-ReactNative-PublicFiles

This is a publuc repo hosting some example code of an app I'm currently working on. 

This is WIP so the code on display will not be within full context of the application.

Thanks for looking!


MedDelivery is an app that you can use to order medicinal marijuana right to your door. Skip the wait of mail deliveries, MedDelivery offers same day delivery, guaranteed.

## Screenshots

![alt text](https://i.imgur.com/L2cUJ66.png)
![alt text](https://imgur.com/Hjxbyzv.png)
![alt text](https://imgur.com/tmFwatg.png)
![alt text](https://imgur.com/THScLif.png)
![alt text](https://imgur.com/CdOBCJj.png)
![alt text](https://imgur.com/RApptyT.png)
![alt text](https://imgur.com/8tCNmc3.png)
![alt text](https://imgur.com/CB0udCP.png)
![alt text](https://i.imgur.com/1VNbDTW.png)
![alt text](https://imgur.com/uEByilq.png)




Inline-style:
![alt text](img/cmd.PNG)

Reference-style:
![alt text1][logo]

[logo]: img/cmd.PNG